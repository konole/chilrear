﻿using Chilrear.Models;
using Chilrear.Responses;
using System;

namespace Chilrear.Services
{
    public class Authentication
    {
        private Boolean accepted = false;
        private IAuthenticableModel Instance { get; set; }
        private String Password;

        public Boolean Accepted
        {
            get
            {
                return accepted;
            }
        }

        public Authentication(IAuthenticableModel obj, String password)
        {
            Instance = obj;
            Password = password;
        }

        public BaseResponse authenticate()
        {
            if (Instance != null && Instance.validatePassword(Password))
            {
                Instance.generateAccessToken();
                accepted = true;
                return new CustomResponse("access_token", Instance.AccessToken);
            }
            else
            {
                return new ErrorResponse("Wrong email or password");
            }
        }
    }
}
