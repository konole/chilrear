﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chilrear.Models;

namespace Chilrear.Presenters
{
    public class UserPresenter : BasePresenter<User>
    {
        public UserPresenter(User u) : base(u) { }

        public override dynamic Attributes
        {
            get
            {
                return new
                {
                    id               = o.Id,
                    username         = o.Username,
                    first_name       = o.FirstName,
                    last_name        = o.LastName,
                    created_at       = o.CreatedAt,
                    statuses         = o.Statuses.OrderByDescending(s => s.CreatedAt).Take(5).Select(s => new StatusPresenter(s).NestedAttributes),
                    starred_statuses = o.StarredStatuses.OrderByDescending(s => s.CreatedAt).Take(5).Select(s => new StatusPresenter(s).NestedAttributes),
                    following        = o.Following.Take(5).ToList<User>().Select(f => new UserPresenter(f).NestedAttributes),
                    following_count  = o.Following.Count,
                    followers        = o.Followers.Take(5).ToList<User>().Select(f => new UserPresenter(f).NestedAttributes),
                    followers_count  = o.Followers.Count
                };
            }
        }

        public dynamic ProfileAttributes
        {
            get
            {
                return new
                {
                    id               = o.Id,
                    email            = o.Email,
                    username         = o.Username,
                    first_name       = o.FirstName,
                    last_name        = o.LastName,
                    created_at       = o.CreatedAt,
                    statuses         = o.Statuses.OrderByDescending(s => s.CreatedAt).Take(5).Select(s => new StatusPresenter(s).NestedAttributes),
                    starred_statuses = o.StarredStatuses.OrderByDescending(s => s.CreatedAt).Take(5).Select(s => new StatusPresenter(s).NestedAttributes),
                    following        = o.Following.Take(5).ToList<User>().Select(f => new UserPresenter(f).NestedAttributes),
                    following_count  = o.Following.Count,
                    followers        = o.Followers.Take(5).ToList<User>().Select(f => new UserPresenter(f).NestedAttributes),
                    followers_count  = o.Followers.Count
                };
            }
        }

        public dynamic NestedAttributes
        {
            get
            {
                return new
                {
                    id              = o.Id,
                    username        = o.Username,
                    first_name      = o.FirstName,
                    last_name       = o.LastName,
                    created_at      = o.CreatedAt,
                    following_count = o.Following.Count,
                    followers_count = o.Followers.Count
                };
            }
        }

        public dynamic UpdateAttributes
        {
            get
            {
                return new
                {
                    id              = o.Id,
                    username        = o.Username,
                    email           = o.Email,
                    first_name      = o.FirstName,
                    last_name       = o.LastName,
                    created_at      = o.CreatedAt,
                };
            }
        }
    }
}