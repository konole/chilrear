﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chilrear.Models;

namespace Chilrear.Presenters
{
    public class StatusPresenter : BasePresenter<Status>
    {
        public StatusPresenter(Status s) : base(s) { }

        public override dynamic Attributes
        {
            get
            {
                return new
                {
                    id = o.Id,
                    body = o.Body,
                    created_at = o.CreatedAt,
                    starred_by = o.StarredBy.Take(10).Select(u => new UserPresenter(u).NestedAttributes),
                    starred_by_count = o.StarredBy.Count,
                    user      = new UserPresenter(o.User).NestedAttributes
                };
            }
        }

        public dynamic TimelineAttributes
        {
            get
            {
                return new
                {
                    id = o.Id,
                    body = o.Body,
                    created_at = o.CreatedAt,
                    starred_by = o.StarredBy.Take(10).Select(u => new UserPresenter(u).NestedAttributes),
                    starred_by_count = o.StarredBy.Count,
                    user = new UserPresenter(o.User).NestedAttributes
                };
            }
        }

        public dynamic NestedAttributes
        {
            get
            {
                return new
                {
                    id = o.Id,
                    body = o.Body,
                    created_at = o.CreatedAt
                };
            }
        }
    }
}