﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chilrear.Models;

namespace Chilrear.Presenters
{
    public abstract class BasePresenter<T>
    {
        protected T o { get; set; }

        public BasePresenter(T instance)
        {
            o = instance;
        }

        public abstract dynamic Attributes { get; }
    }
}