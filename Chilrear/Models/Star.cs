﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Chilrear.Models
{
    public class Star : ModelBase
    {
        public Star() : base() { }

        [Column("status_id")]
        public Status Status { get; set; }

        [Column("user_id")]
        public User User { get; set; }
    }
}