﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Chilrear.Models.Partials
{
    public class UserUpdate : User
    {
        [JsonProperty("password")]
        override public String Password { get; set; }

        override public String AccessToken { get; set; }

        override public String Email { get; set; }

        override public String FirstName { get; set; }

        override public String LastName { get; set; }

        override public String Username { get; set; }
    }
}