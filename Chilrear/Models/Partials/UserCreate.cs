﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Chilrear.Models.Partials
{
    public class UserCreate :User
    {
        [Required]
        [JsonProperty("password")]
        override public string Password { get; set; }

        override public string AccessToken { get; set; }

        [Required]
        override public String Email { get; set; }

        override public String FirstName { get; set; }

        override public String LastName { get; set; }

        [Required]
        override public String Username { get; set; }
    }
}