﻿using Newtonsoft.Json;
using System;
using Chilrear.Models.Partials;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Chilrear.Models.Partials
{
    public class StatusCreate : Status
    {
        [Required]
        override public String Body { get; set; }
    }
}