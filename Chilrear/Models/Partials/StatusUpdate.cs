﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chilrear.Models.Partials
{
    public class StatusUpdate : Status
    {
        public override string Body { get; set; }
    }
}