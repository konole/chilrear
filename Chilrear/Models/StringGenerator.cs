﻿using System;
using System.Linq;

namespace Chilrear.Models
{
    public class StringGenerator
    {
        public static String generate()
        {
            return generate(32);
        }

        public static String generate(int n)
        {
            return generateString(n);
        }

        private static String generateString(int n)
        {
            var chars = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new String(
                Enumerable.Repeat(chars, n)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }
    }
}