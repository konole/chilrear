﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Chilrear.Models
{
    public class Follower : ModelBase
    {
        public Follower() : base() { }

        [Column("follower_id")]
        public User FollowerUser { get; set; }

        [Column("user_id")]
        public User User { get; set; }
    }
}