﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Chilrear.Models.Partials;

namespace Chilrear.Models
{
    public class User : ModelBase, IAuthenticableModel
    {
        [Column("password")]
        [JsonProperty("password")]
        public virtual string Password  { get; set; }

        [Column("access_token")]
        [JsonProperty("access_token")]
        public virtual string AccessToken { get; set; }

        [Column("email")]
        [JsonProperty("email")]
        [MaxLength(255)]
        public virtual String Email { get; set; }

        [Column("first_name")]
        [JsonProperty("first_name")]
        [MaxLength(255)]
        public virtual String FirstName { get; set; }

        [Column("last_name")]
        [JsonProperty("last_name")]
        [MaxLength(255)]
        public virtual String LastName { get; set; }

        [Column("username")]
        [JsonProperty("username")]
        [MaxLength(32)]
        public virtual String Username { get; set; }

        [JsonProperty("statuses")]
        public virtual ICollection<Status> Statuses { get; set; }

        [JsonProperty("followers")]
        public virtual ICollection<User> Followers { get; set; }

        [JsonProperty("following")]
        public virtual ICollection<User> Following { get; set; }

        [JsonProperty("starred_statuses")]
        public virtual ICollection<Status> StarredStatuses { get; set; }

        public User() : base()
        {
            Followers = new List<User>();
            Following = new List<User>();
            StarredStatuses = new List<Status>();
            Statuses = new List<Status>();  
        }

        public void setPassword(String newPassword)
        {
            String salt = BCrypt.Net.BCrypt.GenerateSalt(12);
            Password = BCrypt.Net.BCrypt.HashPassword(newPassword, salt);
        }

        public Boolean validatePassword(String passedPassword)
        {
            return BCrypt.Net.BCrypt.Verify(passedPassword, Password);
        }

        public void generateAccessToken()
        {
            AccessToken = StringGenerator.generate();
        }

        public void Update(UserUpdate user)
        {
            base.Update();

            if (user.Password != null && !user.Password.Equals("")) 
            {
                setPassword(user.Password);
                generateAccessToken();
            }

            if (user.Email != null)
            {
                Email = user.Email;
            }

            if (user.FirstName != null)
            {
                FirstName = user.FirstName;
            }

            if (user.LastName != null)
            {
                LastName = user.LastName;
            }

            if (user.Username != null)
            {
                Username = user.Username;
            }
        }
    }
}