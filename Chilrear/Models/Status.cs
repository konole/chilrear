﻿using Newtonsoft.Json;
using System;
using Chilrear.Models.Partials;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Chilrear.Models
{
    public class Status : ModelBase
    {
        [Column("user_id")]
        [JsonProperty("user_id")]
        public virtual User User { get; set; }

        [Column("body")]
        [JsonProperty("body")]
        [MaxLength(160)]
        public virtual String Body { get; set; }

        [JsonProperty("starred_by")]
        public virtual ICollection<User> StarredBy { get; set; }

        public Status() : base()
        {
            StarredBy = new List<User>();
        }

        public void Update(StatusUpdate status)
        {
            if (status.Body != null)
            {
                base.Update();
                Body = status.Body;
            }
        }
    }
}