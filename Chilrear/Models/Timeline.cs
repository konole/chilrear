﻿using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Data.Objects;

namespace Chilrear.Models
{
    public class Timeline
    {
        private User User { get; set; }

        public Timeline(User user)
        {
            User = user;
        }

        public List<Status> Statuses
        {
            get
            {
                var x = UserStatuses.Concat(FollowingStatuses)
                    .OrderByDescending(s => s.CreatedAt)
                    .Take(100);

                return x
                    .ToList<Status>();
            }
        }

        private IEnumerable<Status> UserStatuses
        {
            get
            {
                return User.Statuses.ToList<Status>();
            }
        }

        private IEnumerable<Status> FollowingStatuses
        {
            get
            {
                return User.Following.Select(u => u.Statuses)
                    .SelectMany(x => x);
            }
        }
    }
}