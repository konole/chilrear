﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Chilrear.Models
{
    public abstract class ModelBase
    {
        public ModelBase()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = CreatedAt;
        }

        [Key]
        [JsonProperty("id")]
        public int Id { get; set; }

        [Column("created_at")]
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [Column("updated_at")]
        [JsonProperty("updated_at")]
        public DateTime UpdatedAt { get; set; }

        public void Update()
        {
            UpdatedAt = DateTime.Now;
        }
    }
}