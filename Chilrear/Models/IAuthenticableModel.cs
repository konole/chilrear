﻿using System;

namespace Chilrear.Models
{
    public interface IAuthenticableModel
    {
        Boolean validatePassword(String passedPassword);
        String AccessToken { get; set; }
        void generateAccessToken();
    }
}