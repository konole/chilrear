﻿using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Routing;

namespace Chilrear
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore; 

            config.Routes.MapHttpRoute(
                "Timeline",
                "api/timeline",
                new
                {
                    controller = "Timeline",
                    action = "Index"
                }
            );

            config.Routes.MapHttpRoute(
                name: "MeFOLLOW",
                routeTemplate: "api/me/follow/{userToFollowId}",
                defaults: new
                {
                    controller = "Profile",
                    action = "Follow"
                },
                constraints: new
                {
                    httpMethod = new HttpMethodConstraint("PUT")
                }
            );

            config.Routes.MapHttpRoute(
                name: "MeUNFOLLOW",
                routeTemplate: "api/me/unfollow/{userToFollowId}",
                defaults: new
                {
                    controller = "Profile",
                    action = "Unfollow"
                },
                constraints: new
                {
                    httpMethod = new HttpMethodConstraint("PUT")
                }
            );

            config.Routes.MapHttpRoute(
                name: "MeGET",
                routeTemplate: "api/me",
                defaults: new
                {
                    controller = "Profile",
                    action = "Index"
                },
                constraints: new 
                {
                    httpMethod = new HttpMethodConstraint("GET")
                }
            );

            config.Routes.MapHttpRoute(
                name: "MePUT",
                routeTemplate: "api/me",
                defaults: new
                {
                    controller = "Profile",
                    action = "Update"
                },
                constraints: new
                {
                    httpMethod = new HttpMethodConstraint("PUT")
                }
            );

            config.Routes.MapHttpRoute(
                name: "MeDELETE",
                routeTemplate: "api/me",
                defaults: new
                {
                    controller = "Profile",
                    action = "Delete"
                },
                constraints: new
                {
                    httpMethod = new HttpMethodConstraint("DELETE")
                }
            );

            config.Routes.MapHttpRoute(
                name: "StatusesINDEX",
                routeTemplate: "api/me/statuses",
                defaults: new
                {
                    controller = "Statuses",
                    action = "Index"
                },
                constraints: new
                {
                    httpMethod = new HttpMethodConstraint("GET")
                }
            );

            config.Routes.MapHttpRoute(
                name: "StatusesSHOW",
                routeTemplate: "api/me/statuses/{id}",
                defaults: new
                {
                    controller = "Statuses",
                    action = "Show"
                },
                constraints: new
                {
                    httpMethod = new HttpMethodConstraint("GET")
                }
            );

            config.Routes.MapHttpRoute(
                name: "StatusesDELETE",
                routeTemplate: "api/me/statuses/{id}",
                defaults: new
                {
                    controller = "Statuses",
                    action = "DeleteStatus"
                },
                constraints: new
                {
                    httpMethod = new HttpMethodConstraint("DELETE")
                }
            );

            config.Routes.MapHttpRoute(
                name: "StatusesPUT",
                routeTemplate: "api/me/statuses/{id}",
                defaults: new
                {
                    controller = "Statuses",
                    action = "PutStatus"
                },
                constraints: new
                {
                    httpMethod = new HttpMethodConstraint("PUT")
                }
            );

            config.Routes.MapHttpRoute(
                name: "StatusStar",
                routeTemplate: "api/statuses/{id}/star",
                defaults: new
                {
                    controller = "Statuses",
                    action = "Star"
                },
                constraints: new
                {
                    httpMethod = new HttpMethodConstraint("PUT")
                }
            );

            config.Routes.MapHttpRoute(
                name: "StatusUnstar",
                routeTemplate: "api/statuses/{id}/unstar",
                defaults: new
                {
                    controller = "Statuses",
                    action = "Unstar"
                },
                constraints: new
                {
                    httpMethod = new HttpMethodConstraint("PUT")
                }
            );

            config.Routes.MapHttpRoute(
                name: "StatusesPOST",
                routeTemplate: "api/me/statuses",
                defaults: new
                {
                    controller = "Statuses",
                    action = "PostStatus"
                },
                constraints: new
                {
                    httpMethod = new HttpMethodConstraint("POST")
                }
            );




            

            config.Routes.MapHttpRoute(
                "AuthApi",
                "api/auth",
                new
                {
                    controller = "Auth",
                    action = "Authenticate",
                }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "UsersResources",
                routeTemplate: "api/users/{userId}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            config.EnableSystemDiagnosticsTracing();
        }
    }
}
