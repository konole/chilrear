﻿using Chilrear.Models;
using System.Data.Entity;

namespace Chilrear
{
    public class EntityContext : DbContext
    {
        public EntityContext() : base("name=ChilrearConnectionString") { } 

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasMany(u => u.Statuses).WithOptional(s => s.User).Map(c => c.MapKey("user_id"));

            modelBuilder.Entity<User>().HasMany(u => u.Followers)
                .WithMany(p => p.Following)
                .Map(w => w.ToTable("Followers").MapLeftKey("user_id").MapRightKey("follower_id"));

            modelBuilder.Entity<User>().HasMany(u => u.StarredStatuses)
                .WithMany(s => s.StarredBy).Map(w => w.ToTable("Stars").MapLeftKey("user_id").MapRightKey("status_id"));

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Status> Status { get; set; }
        public DbSet<User> User { get; set; }
    }
}