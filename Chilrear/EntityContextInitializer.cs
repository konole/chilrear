﻿using Chilrear.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace Chilrear
{
    public class EntityContextInitializer : DropCreateDatabaseAlways<EntityContext>
    {
        protected override void Seed(EntityContext context)
        {
            User user1 = new User { Id = 1, Email = "test@test.com", Username = "tweet", FirstName = "Foo", LastName = "Bar", AccessToken = "test" };
            User user2 = new User { Id = 2, Email = "test2@test.com", Username = "mike", FirstName = "Mike", AccessToken = "test2" };
            User user3 = new User { Id = 3, Email = "test3@test.com", Username = "joedoe", FirstName = "Joe", LastName = "Doe", AccessToken = "test3" };
                
            List<User> users = new List<User>
            {
                user1, user2, user3 
            };


            user1.Followers.Add(user2);

            user1.Followers.Add(user3);

            user1.Following.Add(user2);

            // add data into context and save to db
            foreach (User u in users)
            {
                u.setPassword("test");
                for (int i = 0; i < 3; i++)
                {
                    u.Statuses.Add(new Status { Body = String.Format("#{0} Status by {1}", i, u.Username) });
                }
                context.User.Add(u);
            }

            Status s = new Status { Body = String.Format("Epic Status by {0}", user1.Username) };
            user1.Statuses.Add(s);

            user2.StarredStatuses.Add(s);

            context.SaveChanges();

        }
    }
}