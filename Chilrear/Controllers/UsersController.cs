﻿using Chilrear.Models;
using Chilrear.Models.Partials;
using Chilrear.Presenters;
using Chilrear.Responses;
using System;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Chilrear.Controllers
{
    public class UsersController : BaseController
    {
        private User AccessedUser;

        public UsersController() : base() {}

        // GET api/Users
        public dynamic Get()
        {
            return db.User.ToList<User>().Select(u => new UserPresenter(u).Attributes);
        }

        // GET api/Users/5
        public dynamic Get(long id)
        {
            this.AccessedUser = getUser(id);

            if (User == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return new UserPresenter(AccessedUser).Attributes;
        }

        // POST api/Users
        public dynamic PostUser(UserCreate user)
        {
            if (ModelState.IsValid)
            {
                user.setPassword(user.Password);

                db.User.Add(user);
                db.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.Created, user);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        private User getUser(long id)
        {
            return db.User.Find(id);
        }
    }
}