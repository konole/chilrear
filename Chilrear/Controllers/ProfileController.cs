﻿using Chilrear.Models;
using Chilrear.Models.Partials;
using Chilrear.Presenters;
using Chilrear.Responses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Chilrear.Controllers
{
    public class ProfileController : BaseController
    {
        private User user;
        BaseResponse response;

        // GET api/me
        [HttpGet]
        public dynamic Index()
        {
            if (AuthorizedUser == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return new UserPresenter(AuthorizedUser).ProfileAttributes;
        }

        // PUT api/me
        [HttpPut]
        public dynamic Update(UserUpdate user)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }       

            AuthorizedUser.Update(user);

            db.Entry(AuthorizedUser).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }

            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return new UserPresenter(AuthorizedUser).ProfileAttributes;
        }

        // DELETE api/me
        [HttpDelete]
        public HttpResponseMessage Delete()
        {
            if (AuthorizedUser == null)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }

            db.User.Remove(AuthorizedUser);

            try
            {
                db.SaveChanges();
            }

            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, user);
        }

        [HttpPut]
        public dynamic Follow(int userToFollowId)
        {
            if (AuthorizedUser == null)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }

            User userToFollow = db.User.Find(userToFollowId);

            if (userToFollow == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            AuthorizedUser.Following.Add(userToFollow);

            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPut]
        public dynamic Unfollow(int userToFollowId)
        {
            if (AuthorizedUser == null)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }

            if (AuthorizedUser.Id == userToFollowId)
            {
                response = new ErrorResponse("You can't follow yourself");
                return Request.CreateResponse<BaseResponse>(HttpStatusCode.BadRequest, response);
            }

            User userToFollow = db.User.Find(userToFollowId);

            if (userToFollow == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            AuthorizedUser.Following.Remove(userToFollow);

            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }



        protected User AuthorizedUser
        {
            get
            {
                if (user == null)
                {
                    String accessToken = Params["access_token"];
                    user = db.User.Where(u => u.AccessToken == accessToken).FirstOrDefault();
                }
                return user;
            }
        }
    }
}
