﻿using Chilrear.Models;
using Chilrear.Responses;
using Chilrear.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Chilrear.Helpers;

namespace Chilrear.Controllers
{
    [AllowCrossSiteJson]
    public class AuthController : BaseController
    {
        [HttpPost]
        public HttpResponseMessage Authenticate(Dictionary<String, String> authParams)
        {
            String login, password;
            BaseResponse response;
            
            try
            {
                if (authParams.ContainsKey("email"))
                {
                    login = authParams["email"];
                }
                else
                {
                    login = authParams["username"];
                }

                password = authParams["password"];
            }
            catch
            {
                response = new ErrorResponse("You need to pass email / username & password params");
                return Request.CreateResponse<BaseResponse>(HttpStatusCode.BadRequest, response);
            }

            User user = getUser(login);

            Authentication auth = new Authentication(user, password);

            response = auth.authenticate();
            db.SaveChanges(); // Save new token

            if (auth.Accepted) {
                return Request.CreateResponse<BaseResponse>(HttpStatusCode.OK, response);
            }  else {
                return Request.CreateResponse<BaseResponse>(HttpStatusCode.Forbidden, response);
            }

        }

        private User getUser(String login)
        {
            User user = db.User.Where(u => u.Email == login || u.Username == login).FirstOrDefault();

            return user;
        }
    }
}
