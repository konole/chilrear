﻿using Chilrear.Models;
using Chilrear.Models.Partials;
using Chilrear.Presenters;
using System;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Chilrear.Controllers
{
    public class StatusesController : BaseController
    {
        private Status status;
        private User user;
        private int UserId = 0;

        public StatusesController() : base() { }

        [HttpGet]
        public dynamic Index()
        {
            return Get(-1);
        }

        [HttpGet]
        public dynamic Show(int id)
        {
            return Get(-1, id);
        }

        [HttpGet]
        public dynamic Get(int userId)
        {
            UserId = userId;

            if (UserId == -1)
            {
                return db.Status
                    .OrderByDescending(s => s.CreatedAt)
                    .Take(100)
                    .ToList<Status>()
                    .Select(s => new StatusPresenter(s).Attributes);
            }

            if (User == null)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }

            return User.Statuses.Select(s => new StatusPresenter(s).Attributes);
        }

        [HttpGet]
        public dynamic Get(int userId, int id)
        {
            UserId = userId;

            if (UserId == -1)
            {
                status = getStatus(id);
            }
            else if (User == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            status = getStatus(id);

            if (status == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return new StatusPresenter(status).Attributes;
        }

        [HttpPut]
        public dynamic Star(int id)
        {
            if (User == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            status = getStatus(id);

            if (!status.StarredBy.Contains(User))
            {
                status.StarredBy.Add(User);
            }

            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPut]
        public dynamic Unstar(int id)
        {
            if (User == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            status = getStatus(id);

            status.StarredBy.Remove(User);

            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPut]
        public HttpResponseMessage PutStatus(int id, StatusUpdate updatedStatus)
        {
            if (User == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Forbidden));
            }

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            this.status = getStatus(id, User);

            if (status == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            if ((updatedStatus.Id != 0 && updatedStatus.Id != id))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            status.Update(updatedStatus);

            db.Entry(status).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        public dynamic PostStatus(Status status)
        {
            if (User == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Forbidden));
            }

            if (ModelState.IsValid)
            {
                User.Statuses.Add(status);
                db.SaveChanges();

                return new StatusPresenter(status).Attributes;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpDelete]
        public HttpResponseMessage DeleteStatus(int id)
        {
            if (User == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Forbidden));
            }

            this.status = getStatus(id, User);

            if (status == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            User.Statuses.Remove(status);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, status);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        private new User User
        {
            get
            {
                if (user == null)
                {
                    user = (AuthorizedUser != null) ? AuthorizedUser : db.User.Where(u => u.Id == UserId).FirstOrDefault();
                }

                return user;
            }
        }

        private Status getStatus(int id, User _user = null)
        {
            return (_user == null) ? db.Status.Find(id) : _user.Statuses.Where(s => s.Id == id).FirstOrDefault();
        }

        protected User AuthorizedUser
        {
            get
            {
                if (Params.Keys.Contains("access_token"))
                {
                    String accessToken = Params["access_token"];
                    return db.User.Where(u => u.AccessToken == accessToken).FirstOrDefault();
                }

                return null;
            }
        }
    }
}