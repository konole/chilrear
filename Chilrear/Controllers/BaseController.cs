﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Chilrear.Helpers;

namespace Chilrear.Controllers
{
    [AllowCrossSiteJson]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public abstract class BaseController : ApiController
    {
        protected EntityContext db;
        protected Dictionary<String, String> queryParams;

        public BaseController()
        {
            db = new EntityContext();
        }

        public Dictionary<String, String> Params
        {
            get
            {
                if (queryParams == null)
                {
                    var parsedParams = HttpUtility.ParseQueryString(Request.RequestUri.Query);

                    queryParams = new Dictionary<String, String>();

                    foreach (string key in parsedParams.Keys)
                    {
                        queryParams.Add(key, parsedParams[key]);
                    }
                }

                return queryParams;
            }
        }
    }
}
