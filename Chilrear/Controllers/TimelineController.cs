﻿using Chilrear.Models;
using Chilrear.Responses;
using Chilrear.Presenters;
using System;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Chilrear.Controllers
{
    public class TimelineController : BaseController
    {
        private User user;

        [HttpGet]
        public dynamic Index()
        {
            if (AuthorizedUser == null)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, new ErrorResponse("Unauthorized request"));
            }

            Timeline timeline = new Timeline(AuthorizedUser);

            return timeline.Statuses.Select(s => new StatusPresenter(s).TimelineAttributes);
        }

        protected User AuthorizedUser
        {
            get
            {
                if (user == null)
                {
                    String accessToken = Params["access_token"];
                    user = db.User.Where(u => u.AccessToken == accessToken).FirstOrDefault();
                }
                return user;
            }
        }
    }
}
