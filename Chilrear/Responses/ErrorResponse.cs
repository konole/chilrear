﻿using System;

namespace Chilrear.Responses
{
    public class ErrorResponse : BaseResponse
    {
        public ErrorResponse(String error) : base()
        {
            this.Add("error", error);
        }
    }
}