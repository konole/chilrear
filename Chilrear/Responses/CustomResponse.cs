﻿using System;

namespace Chilrear.Responses
{
    public class CustomResponse : BaseResponse
    {
        public CustomResponse(String key, String value) : base()
        {
            this.Add(key, value);
        }
    }
}
